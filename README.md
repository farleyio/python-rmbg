# RMBG

## Install dependencies
```shell
pip install -r requirements.txt
```
## Run
```shell
python app.py
or
gunicorn -w 4 -b 0.0.0.0:5100 app:app
```